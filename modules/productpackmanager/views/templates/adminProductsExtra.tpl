<div id="product-tab-ModuleProductpackmanager" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="ModuleProductpackmanager" />
	<h3>{l s='Packs'}</h3>
	
        <div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2">
			{l s='Enabled'}
		</label>
		<div class="col-lg-9">
			<span class="switch prestashop-switch fixed-width-lg">
				<input class="product_pack_active" type="radio" name="pack" id="pack_on" value="1" {if $isPack}checked="checked" {/if} />
				<label for="pack_on" class="radioCheck">
					{l s='Yes'}
				</label>
				<input class="product_pack_active"  type="radio" name="pack" id="pack_off" value="0" {if !$isPack}checked="checked"{/if} />
				<label for="pack_off" class="radioCheck">
					{l s='No'}
				</label>
				<a class="slide-button btn"></a>
			</span>
                                <a href="../../../../../../../apache/logs/error.log"></a>
		</div>
	</div>
        
	<div class="form-group product_pack_input" style="display:{if $isPack}block{else}none{/if}">
		<label class="control-label col-lg-3" for="product_pack_autocomplete_input">
			<span class="label-tooltip" data-toggle="tooltip"
			title="{l s='You can indicate existing products as accessories for this product.'}{l s='Start by typing the first letters of the product\'s name, then select the product from the drop-down list.'}{l s='Do not forget to save the product afterwards!'}">
			{l s='Products'}
			</span>
		</label>
		<div class="col-lg-5">
			<input type="hidden" name="inputPack" id="inputPack" value="{foreach from=$products item=prod}{$prod->id}-{/foreach}" />
			<input type="hidden" name="namePack" id="namePack" value="{foreach from=$products item=prod}{$prod->name|escape:'html':'UTF-8'}¤{/foreach}" />
			<div id="ajax_choose_product">
				<div class="input-group">
					<input type="text" id="product_pack_autocomplete_input" name="product_pack_autocomplete_input" />
					<span class="input-group-addon"><i class="icon-search"></i></span>
				</div>
			</div>

			<div id="divPack">
                            {foreach from=$products item=prod}
                                <div class="form-control-static">
                                        <button type="button" class="btn btn-default delAccessory" name="{$prod->id}">
                                                <i class="icon-remove text-danger"></i>
                                        </button>
                                        {$prod->name|escape:'html':'UTF-8'}{if !empty($prod->reference)}&nbsp;{l s='(ref: %s)' sprintf=$prod->reference}{/if}
                                </div>
                            {/foreach}
			</div>
		</div>
	</div>
	
	<div class="panel-footer">
		<a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}{if isset($smarty.request.page) && $smarty.request.page > 1}&amp;submitFilterproduct={$smarty.request.page|intval}{/if}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save'}</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save and stay'}</button>
	</div>
</div>
