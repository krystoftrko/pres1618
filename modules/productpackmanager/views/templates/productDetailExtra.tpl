{if isset($packItems) && $packItems|@count > 0}
    <section id="blockpack">
	<h3 class="page-product-heading">{l s='Pack content'}</h3>
	{include file="$tpl_dir./product-list.tpl" products=$packItems}
    </section>
{/if}