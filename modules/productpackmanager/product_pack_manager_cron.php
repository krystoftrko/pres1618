<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

if(Module::isEnabled('productpackmanager') && $module = Module::getInstanceByName('productpackmanager')){
    $module->cron();
}