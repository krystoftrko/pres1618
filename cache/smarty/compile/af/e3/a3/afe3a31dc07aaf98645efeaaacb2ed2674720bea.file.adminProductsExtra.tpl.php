<?php /* Smarty version Smarty-3.1.19, created on 2018-10-18 23:00:17
         compiled from "D:\KT\Weboservis\pres1618\modules\productpackmanager\views\templates\adminProductsExtra.tpl" */ ?>
<?php /*%%SmartyHeaderCode:130615bc8f4616fc1d3-23389751%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'afe3a31dc07aaf98645efeaaacb2ed2674720bea' => 
    array (
      0 => 'D:\\KT\\Weboservis\\pres1618\\modules\\productpackmanager\\views\\templates\\adminProductsExtra.tpl',
      1 => 1539885272,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '130615bc8f4616fc1d3-23389751',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'isPack' => 0,
    'products' => 0,
    'prod' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5bc8f461787b09_84252957',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5bc8f461787b09_84252957')) {function content_5bc8f461787b09_84252957($_smarty_tpl) {?><div id="product-tab-ModuleProductpackmanager" class="panel product-tab">
	<input type="hidden" name="submitted_tabs[]" value="ModuleProductpackmanager" />
	<h3><?php echo smartyTranslate(array('s'=>'Packs'),$_smarty_tpl);?>
</h3>
	
        <div class="form-group">
		<div class="col-lg-1"><span class="pull-right"></span></div>
		<label class="control-label col-lg-2">
			<?php echo smartyTranslate(array('s'=>'Enabled'),$_smarty_tpl);?>

		</label>
		<div class="col-lg-9">
			<span class="switch prestashop-switch fixed-width-lg">
				<input class="product_pack_active" type="radio" name="pack" id="pack_on" value="1" <?php if ($_smarty_tpl->tpl_vars['isPack']->value) {?>checked="checked" <?php }?> />
				<label for="pack_on" class="radioCheck">
					<?php echo smartyTranslate(array('s'=>'Yes'),$_smarty_tpl);?>

				</label>
				<input class="product_pack_active"  type="radio" name="pack" id="pack_off" value="0" <?php if (!$_smarty_tpl->tpl_vars['isPack']->value) {?>checked="checked"<?php }?> />
				<label for="pack_off" class="radioCheck">
					<?php echo smartyTranslate(array('s'=>'No'),$_smarty_tpl);?>

				</label>
				<a class="slide-button btn"></a>
			</span>
                                <a href="../../../../../../../apache/logs/error.log"></a>
		</div>
	</div>
        
	<div class="form-group product_pack_input" style="display:<?php if ($_smarty_tpl->tpl_vars['isPack']->value) {?>block<?php } else { ?>none<?php }?>">
		<label class="control-label col-lg-3" for="product_pack_autocomplete_input">
			<span class="label-tooltip" data-toggle="tooltip"
			title="<?php echo smartyTranslate(array('s'=>'You can indicate existing products as accessories for this product.'),$_smarty_tpl);?>
<?php echo smartyTranslate(array('s'=>'Start by typing the first letters of the product\'s name, then select the product from the drop-down list.'),$_smarty_tpl);?>
<?php echo smartyTranslate(array('s'=>'Do not forget to save the product afterwards!'),$_smarty_tpl);?>
">
			<?php echo smartyTranslate(array('s'=>'Products'),$_smarty_tpl);?>

			</span>
		</label>
		<div class="col-lg-5">
			<input type="hidden" name="inputPack" id="inputPack" value="<?php  $_smarty_tpl->tpl_vars['prod'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['prod']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['prod']->key => $_smarty_tpl->tpl_vars['prod']->value) {
$_smarty_tpl->tpl_vars['prod']->_loop = true;
?><?php echo $_smarty_tpl->tpl_vars['prod']->value->id;?>
-<?php } ?>" />
			<input type="hidden" name="namePack" id="namePack" value="<?php  $_smarty_tpl->tpl_vars['prod'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['prod']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['prod']->key => $_smarty_tpl->tpl_vars['prod']->value) {
$_smarty_tpl->tpl_vars['prod']->_loop = true;
?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prod']->value->name, ENT_QUOTES, 'UTF-8', true);?>
¤<?php } ?>" />
			<div id="ajax_choose_product">
				<div class="input-group">
					<input type="text" id="product_pack_autocomplete_input" name="product_pack_autocomplete_input" />
					<span class="input-group-addon"><i class="icon-search"></i></span>
				</div>
			</div>

			<div id="divPack">
                            <?php  $_smarty_tpl->tpl_vars['prod'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['prod']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['products']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['prod']->key => $_smarty_tpl->tpl_vars['prod']->value) {
$_smarty_tpl->tpl_vars['prod']->_loop = true;
?>
                                <div class="form-control-static">
                                        <button type="button" class="btn btn-default delAccessory" name="<?php echo $_smarty_tpl->tpl_vars['prod']->value->id;?>
">
                                                <i class="icon-remove text-danger"></i>
                                        </button>
                                        <?php echo htmlspecialchars($_smarty_tpl->tpl_vars['prod']->value->name, ENT_QUOTES, 'UTF-8', true);?>
<?php if (!empty($_smarty_tpl->tpl_vars['prod']->value->reference)) {?>&nbsp;<?php echo smartyTranslate(array('s'=>'(ref: %s)','sprintf'=>$_smarty_tpl->tpl_vars['prod']->value->reference),$_smarty_tpl);?>
<?php }?>
                                </div>
                            <?php } ?>
			</div>
		</div>
	</div>
	
	<div class="panel-footer">
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getAdminLink('AdminProducts'), ENT_QUOTES, 'UTF-8', true);?>
<?php if (isset($_REQUEST['page'])&&$_REQUEST['page']>1) {?>&amp;submitFilterproduct=<?php echo intval($_REQUEST['page']);?>
<?php }?>" class="btn btn-default"><i class="process-icon-cancel"></i> <?php echo smartyTranslate(array('s'=>'Cancel'),$_smarty_tpl);?>
</a>
		<button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> <?php echo smartyTranslate(array('s'=>'Save'),$_smarty_tpl);?>
</button>
		<button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> <?php echo smartyTranslate(array('s'=>'Save and stay'),$_smarty_tpl);?>
</button>
	</div>
</div>
<?php }} ?>
