$(document).ready(function () {
    
        if (typeof tabs_manager === 'undefined') {
            return;
        }

        product_tabs['ModuleProductpackmanager'] = new function(){          
                var self = this;

                this.initAccessoriesAutocomplete = function (){
                        $('#product_pack_autocomplete_input')
                                .autocomplete('ajax_products_list.php?exclude_packs=0&excludeVirtuals=0', {
                                        minChars: 1,
                                        autoFill: true,
                                        max:20,
                                        matchContains: true,
                                        mustMatch:false,
                                        scroll:false,
                                        cacheLength:0,
                                        formatItem: function(item) {
                                                return item[1]+' - '+item[0];
                                        }
                                }).result(self.addAccessory);

                        $('#product_pack_autocomplete_input').setOptions({
                                extraParams: {
                                        excludeIds : self.getAccessoriesIds()
                                }
                        });
                };

                this.getAccessoriesIds = function()
                {
                        if ($('#inputPack').val() === undefined)
                                return id_product;
                        return id_product + ',' + $('#inputPack').val().replace(/\-/g,',');
                }

                this.addAccessory = function(event, data, formatted)
                {
                        if (data == null)
                                return false;
                        var productId = data[1];
                        var productName = data[0];

                        var $divAccessories = $('#divPack');
                        var $inputAccessories = $('#inputPack');
                        var $nameAccessories = $('#namePack');

                        /* delete product from select + add product line to the div, input_name, input_ids elements */
                        $divAccessories.html($divAccessories.html() + '<div class="form-control-static"><button type="button" class="delAccessory btn btn-default" name="' + productId + '"><i class="icon-remove text-danger"></i></button>&nbsp;'+ productName +'</div>');
                        $nameAccessories.val($nameAccessories.val() + productName + '¤');
                        $inputAccessories.val($inputAccessories.val() + productId + '-');
                        $('#product_pack_autocomplete_input').val('');
                        $('#product_pack_autocomplete_input').setOptions({
                                extraParams: {excludeIds : self.getAccessoriesIds()}
                        });
                };

                this.delAccessory = function(id)
                {
                        var div = getE('divPack');
                        var input = getE('inputPack');
                        var name = getE('namePack');

                        // Cut hidden fields in array
                        var inputCut = input.value.split('-');
                        var nameCut = name.value.split('¤');

                        if (inputCut.length != nameCut.length)
                                return jAlert('Bad size');

                        // Reset all hidden fields
                        input.value = '';
                        name.value = '';
                        div.innerHTML = '';
                        for (i in inputCut)
                        {
                                // If empty, error, next
                                if (!inputCut[i] || !nameCut[i])
                                        continue ;

                                // Add to hidden fields no selected products OR add to select field selected product
                                if (inputCut[i] != id)
                                {
                                        input.value += inputCut[i] + '-';
                                        name.value += nameCut[i] + '¤';
                                        div.innerHTML += '<div class="form-control-static"><button type="button" class="delAccessory btn btn-default" name="' + inputCut[i] +'"><i class="icon-remove text-danger"></i></button>&nbsp;' + nameCut[i] + '</div>';
                                }
                                else
                                        $('#selectAccessories').append('<option selected="selected" value="' + inputCut[i] + '-' + nameCut[i] + '">' + inputCut[i] + ' - ' + nameCut[i] + '</option>');
                        }

                        $('#product_pack_autocomplete_input').setOptions({
                                extraParams: {excludeIds : self.getAccessoriesIds()}
                        });
                };

                this.onReady = function(){
                        self.initAccessoriesAutocomplete();		
                        $('#divPack').delegate('.delAccessory', 'click', function(){
                                self.delAccessory($(this).attr('name'));
                        });	
                        
                        $('.product_pack_active').click(function(){
                            if($(this).val() === '1'){
                                $('.product_pack_input').fadeIn();
                            }
                            else{
                                $('.product_pack_input').fadeOut();
                            }
                        });
                };
        }

});