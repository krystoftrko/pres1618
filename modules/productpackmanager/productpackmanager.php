<?php

// check PS version
if (!defined('_PS_VERSION_')) {
    exit;
}

class ProductPackManager extends Module{
    
    public function __construct() {
        $this->name = 'productpackmanager';
        $this->tab = 'administrations';
        $this->version = '1.0.1';
        $this->author = 'Weboservis, s.r.o.';
        $this->need_instance = 0;

        parent::__construct();

        $this->displayName = $this->l('Product Pack Manager');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }
    
    public function install(){
        return (parent::install()
                && $this->installDb()
               	&& $this->registerHook('actionValidateOrder')
               	&& $this->registerHook('actionBeforeCartUpdateQty')
               	&& $this->registerHook('displayAdminProductsExtra')
                && $this->registerHook('actionAdminControllerSetMedia')
                && $this->registerHook('actionProductSave')            
                && $this->registerHook('displayFooterProduct')
                && $this->registerHook('displayProductPriceBlock')            
		);
    }
    
    private function installDb(){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute(
                "
                CREATE TABLE `"._DB_PREFIX_."pack_extra` (
                  `id_product` int(10) unsigned NOT NULL,
                  `id_product_item` int(10) unsigned NOT NULL
                ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
                "
        );
    }
    
    public function uninstall() { 
        return parent::uninstall()
                && $this->uninstallDb();
    }
    
    private function uninstallDb(){
        return Db::getInstance(_PS_USE_SQL_SLAVE_)->Execute(
                "
                DROP TABLE IF EXISTS `"._DB_PREFIX_."pack_extra`;
                "
        );
    }
    
    public function hookDisplayProductPriceBlock($params){
        if($params['type'] == 'weight' && Validate::isLoadedObject($params['product'])){

            $priceSum = $this->getProductsInPackPriceSum($params['product']->id);

            if($priceSum && $priceSum > $params['product']->price){
                return $this->l('Namísto ').'<span style="text-decoration: line-through;">'.Tools::displayPrice($priceSum).'</span>';
            }
        } 
    }

    private function getProductsInPackPriceSum($idProduct){
        $db = Db::getInstance();
        
        $q = (new DbQuery)->select('id_product_item')
                ->from('pack_extra')
                ->where('id_product = '.$idProduct);
        
        $inPack = $db->executeS($q);
        
        if(!$inPack){
            return false;
        }
        
        $priceSum = 0;
        
        foreach($inPack as $pack){
            //without attribute!
            $priceSum += (float)Product::getPriceStatic($pack['id_product_item']);
        }
        
        return $priceSum;
    }    

    public function hookDisplayAdminProductsExtra($params){
        $productId = Tools::getValue('id_product');
        
        if(!is_numeric($productId)){
            return '';
        }
        
        $isPack = $this->isPack($productId);
        $products = [];
        
        if($isPack){
            $products = $this->getProductsPack($productId);
        }
        
        $this->smarty->assign([
            'isPack' => $isPack,
            'products' => $products
        ]);
       
        return $this->display(__FILE__, 'views/templates/adminProductsExtra.tpl');
    }
    
    public function hookActionAdminControllerSetMedia($params) {
        $this->context->controller->addJquery();
        $this->context->controller->addJqueryPlugin('autocomplete');
        $this->context->controller->addJS($this->_path.'productpackmanager.js');
    }
 
    public function hookActionProductSave($param) {
        $id_product = $param["id_product"];           
        $packVal = Tools::getValue("inputPack");
        $active = Tools::getValue('pack') == 1;
        
        $this->savePack($packVal, $id_product, $active);
    }
    
    public function hookDisplayFooterProduct($param) {      
        $this->smarty->assign(array(
            'packItems' => $this->getItemTableExtra($param['product']->id),
        ));
        
        return $this->display(__FILE__, 'views/templates/productDetailExtra.tpl');    
    }
    
    private function getItemTableExtra($id_product) {
        $context = $this->context;
        $id_lang = $context->language->id;
        
        $sql = 'SELECT p.*, product_shop.*, pl.*, image_shop.`id_image` id_image, il.`legend`, cl.`name` AS category_default, product_shop.`id_category_default`, p.id_product
				FROM `'._DB_PREFIX_.'pack_extra` a
				LEFT JOIN `'._DB_PREFIX_.'product` p ON p.id_product = a.id_product_item
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON p.id_product = pl.id_product
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
				LEFT JOIN `'._DB_PREFIX_.'image_shop` image_shop
					ON (image_shop.`id_product` = p.`id_product` AND image_shop.cover=1 AND image_shop.id_shop='.(int)$context->shop->id.')
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (image_shop.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
				'.Shop::addSqlAssociation('product', 'p').'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').'
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
				AND a.`id_product` = '.(int)$id_product.'
				';

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        return Product::getProductsProperties($id_lang, $result);
    }
    
    
    public function hookActionValidateOrder($param) {
        $products = $param['order']->getProducts();
        
        foreach($products as $product){
            
            $deltaQuantity = -(int)$product['product_quantity'];
            $inPackProducts = $this->getProductsInPack($product['id_product']);

            if($inPackProducts){
                $updateProducts = $this->getCommonProductAttribute($product['product_attribute_id'], $inPackProducts);
                foreach($updateProducts as $updateProduct){
                    StockAvailable::updateQuantity($updateProduct['productId'], $updateProduct['productAttributeId'], $deltaQuantity);
                }
            }
        }
    }
    
    private function getCommonProductAttribute($productAttributeId, $inPackProducts){
        $attributeIds = $this->getAttributeIds($productAttributeId);
        sort($attributeIds);
        
        $updateProducts = [];
        
        foreach($inPackProducts as $inPackProductId => $inPackProduct){
            foreach($inPackProduct as $idProductAttribute => $attributeIdsInPack){
                
                sort($attributeIdsInPack);
                //Correct combination must be in admin
                if($attributeIds == $attributeIdsInPack){
                    $updateProducts[] = [
                        'productId' => $inPackProductId,
                        'productAttributeId' => $idProductAttribute
                    ];
                }
            }
        }
        return $updateProducts;
    }
    
    private function getAttributeIds($productAttribute){
        $q = (new DbQuery)
                ->select('id_attribute')
                ->from('product_attribute_combination')
                ->where('id_product_attribute = '.$productAttribute);

        $data = [];
        
        foreach(Db::getInstance()->executeS($q) as $dat){
            $data[] = $dat['id_attribute'];
        }
        
        return $data;
    }
    
    private function getProductsInPack($idProduct){
        $db = Db::getInstance();
        
        $q = (new DbQuery)->select('id_product_item')
                ->from('pack_extra')
                ->where('id_product = '.$idProduct);
        
        $inPack = $db->executeS($q);
        
        if(!$inPack){
            return false;
        }
        
        $products = [];
        
        foreach($inPack as $pack){
            
            $qu = (new DbQuery())->select('id_product_attribute')
                    ->from('product_attribute')
                    ->where('id_product = '.$pack['id_product_item']);
            
            foreach($db->executeS($qu) as $productAttribute){
                $products[$pack['id_product_item']][$productAttribute['id_product_attribute']] = $this->getAttributeIds($productAttribute['id_product_attribute']);
            }
            
        }
        
        return $products;
    }    
    
    private function isPack($productId){
        $q = (new DbQuery())
                ->select('COUNT(*)')
                ->from('pack_extra')
                ->where('id_product = '.$productId);
        
        $dat = Db::getInstance()->getRow($q);
        
        return $dat['COUNT(*)'] > 0;
    }
    
    private function getProductsPack($productId){
        $q = (new DbQuery())
                ->select('id_product_item')
                ->from('pack_extra')
                ->where('id_product = '.$productId);
        
        $data = [];
        
        foreach(Db::getInstance()->executeS($q) as $row){
            $data[] = new Product($row['id_product_item'], false, $this->context->language->id);
        }

        return $data;
    }
    
    private function savePack($packVal, $idProduct, $active){
        if(!is_numeric($idProduct)){
            return;
        }
        
        $db = Db::getInstance();
        
        $db->delete('pack_extra', 'id_product = '.$idProduct);
        
        if(!$active){
            return;
        }
        
        foreach(explode('-', $packVal) as $val){
            if(is_numeric($val)){
                $db->insert('pack_extra', [
                    'id_product' => $idProduct,
                    'id_product_item' => $val
                ]);
            }
        }
    }

    public function cron(){
        //for all pack products
        foreach($this->getAllPacks() as $productId){

            //not optimized
            $inPackProducts = $this->getProductsInPack($productId['id_product']);
            $attributes = $this->getProductAttributeIds($productId['id_product']);
            
            //for all attributes
            foreach($attributes as $attribute){
                
                $minQuantity = PHP_INT_MAX;
                $minProductAttribute = 0;

                $updateProducts = $this->getCommonProductAttribute($attribute['id_product_attribute'], $inPackProducts);

                foreach($updateProducts as $updateProduct){
                    $quantity = Product::getRealQuantity($updateProduct['productId'], $updateProduct['productAttributeId']);

                    if($quantity < $minQuantity){
                        $minQuantity = $quantity;
                        $minProductAttribute = $attribute['id_product_attribute'];
                    }
                }

                if($minQuantity !== PHP_INT_MAX && $minProductAttribute !== 0){ 
                    StockAvailable::setQuantity($productId['id_product'], $minProductAttribute, $minQuantity);
                }
            } 
        }
    }

    private function getProductAttributeIds($productId){
        $q = (new DbQuery())
                ->select('id_product_attribute')
                ->from('product_attribute')
                ->where('id_product = '.$productId);

        return Db::getInstance()->executeS($q);
    }

    private function getAllPacks(){
        $q = (new DbQuery())
                ->select('id_product')
                ->groupBy('id_product')
                ->from('pack_extra');

        return Db::getInstance()->executeS($q);
    }
    
}